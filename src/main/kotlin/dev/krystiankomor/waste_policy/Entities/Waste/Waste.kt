package dev.krystiankomor.waste_policy.Entities.Waste

import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import javax.persistence.*


@Entity
data class Waste(
        var name: String = "",

        @OneToOne
        var image: ImageStorage? = null,

        @ManyToOne
        var category: Category? = null,

        @Id
        @GeneratedValue
        var id: Long? = null
)
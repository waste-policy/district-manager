package dev.krystiankomor.waste_policy.Entities.Waste

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import dev.krystiankomor.waste_policy.Entities.Common.ImageStorageType
import javax.persistence.*

enum class CollectorTypeEnum {
        BAG,
        BIN,
        SELECTIVE_WASTES_POINT,
        ELECTRO_WASTES_POINT,
        MOBILE_SELECTIVE_WASTES_POINT,
        COMPANY
}

@Entity
data class Collector(
        var name: String = "",
        var description: String = "",
        var address: String = "",
        var addressDescription: String = "",

        @ManyToOne
        var color: Color? = null,

        @ManyToMany
        var categories: Set<Category> = setOf(),

        @ManyToOne
        var district: District? = null,

        var type: CollectorTypeEnum? = null,

        @ManyToOne
        var image: ImageStorage? = null,

        var isSeasonSpecified: Boolean = false,

        @Id
        @GeneratedValue
        var id: Long? = null
) {
        fun generateImageUrl() {
                if(this.image == null && this.color != null) {
                        this.image = ImageStorage("https://api.krystiankomor.dev/api/generator/${this.type.toString().toLowerCase()}/${this.color?.value}.svg", storageType = ImageStorageType.API)
//                        this.image = ImageStorage("http://localhost:4000/api/generator/${this.type.toString().toLowerCase()}/${this.color?.value}.svg", storageType = ImageStorageType.API)
                }
        }
}

package dev.krystiankomor.waste_policy.Entities.Waste

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
data class Category (
        var name: String = "",
        var description: String = "",

        @Id
        @GeneratedValue
        @Column(name = "id")
        var id: Long? = null
)
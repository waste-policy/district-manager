package dev.krystiankomor.waste_policy.Entities.Common

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

enum class ImageStorageType {
        API, EXTERNAL
}

@Entity
data class ImageStorage(var fileName: String = "", var storageType: ImageStorageType = ImageStorageType.EXTERNAL){
        @Id
        @GeneratedValue
        @Column(name = "id")
        var id: Long = 0
}
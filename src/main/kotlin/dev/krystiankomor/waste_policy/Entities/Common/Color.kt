package dev.krystiankomor.waste_policy.Entities.Common

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Color(var name: String = "", var value: String = ""){
        @Id
        @GeneratedValue
        var id: Long = 0
}
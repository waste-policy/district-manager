package dev.krystiankomor.waste_policy.Entities.User

import javax.persistence.*


@Entity
data class Role(
        var name: String = "",

        @OneToMany(mappedBy = "role")
        var users: Set<User>? = null,

        @Id
        @GeneratedValue
        var id: Long? = null
)
package dev.krystiankomor.waste_policy.Entities.User

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import javax.persistence.*


@Entity
data class User(
        var email: String = "",
        var password: String = "",

        @ManyToOne
        var role: Role? = null,

        @ManyToOne
        var district: District? = null,

        @Id
        @GeneratedValue
        var id: Long? = null
)
package dev.krystiankomor.waste_policy.Entities.AdministrativeDivision

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
data class Voivodeship (
        val name: String = "",

        @Id
        @GeneratedValue
        var id: Long = 0
)
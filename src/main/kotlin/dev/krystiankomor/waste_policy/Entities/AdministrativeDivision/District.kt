package dev.krystiankomor.waste_policy.Entities.AdministrativeDivision

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne


@Entity
data class District(
        var name: String = "",

        @ManyToOne
        var voivodeship: Voivodeship = Voivodeship(),

        @Id
        @GeneratedValue
        var id: Long = 0
)
package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import dev.krystiankomor.waste_policy.Entities.Waste.Waste

data class Game(var wastes: List<Waste>,
           var wasteCollectors: List<Collector>,
           var stages: List<Stage>,
           var district: District
           )
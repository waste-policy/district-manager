package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne


@Entity
data class CollectorWithPosition(
        @ManyToOne
        var wasteCollector: Collector? = null,

        override var x: Long = 0,
        override var y: Long = 0,
        override var scale: Long = 0,
        override var zIndex: Long = 0,
        override var countFromRight: Boolean = false,
        override var countFromBottom: Boolean = false,

        @Id
        @GeneratedValue
        var id: Long? = null
) : ObjectPosition

class CollectorWithPositionForRest(collectorWithPosition: CollectorWithPosition){
        var wasteCollectorId = collectorWithPosition.wasteCollector?.id
        var x = collectorWithPosition.x
        var y = collectorWithPosition.y
        var scale = collectorWithPosition.scale
        var countFromRight = collectorWithPosition.countFromRight
        var countFromBottom = collectorWithPosition.countFromBottom
}
package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.Waste.Waste
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class WasteWithPosition(
        @ManyToOne
        var waste: Waste? = null,

        override var x: Long = 0,
        override var y: Long = 0,
        override var scale: Long = 0,
        override var zIndex: Long = 0,
        override var countFromRight: Boolean = false,
        override var countFromBottom: Boolean = false,

        @Id
        @GeneratedValue
        var id: Long? = null
) : ObjectPosition

class WasteWithPositionForRest(wasteWithPosition: WasteWithPosition){
        var wasteId = wasteWithPosition.waste?.id
        var x = wasteWithPosition.x
        var y = wasteWithPosition.y
        var scale = wasteWithPosition.scale
        var countFromRight = wasteWithPosition.countFromRight
        var countFromBottom = wasteWithPosition.countFromBottom
}
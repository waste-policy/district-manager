package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import javax.persistence.*


@Entity
data class Stage(
        @ManyToMany
        var backgrounds: Set<BackgroundWithPosition>? = null,

        @ManyToMany
        var wastes: Set<WasteWithPosition>? = null,

        @OneToOne
        var standardWasteCollectorsPosition: StandartObjectPosition? = null,

        @ManyToMany
        var wasteCollectors: Set<CollectorWithPosition>? = null,

        @ManyToOne
        var district: District? = null,

        var stageNumber: Long = 0,

        @Id
        @GeneratedValue
        var id: Long? = null
        )


class StageForRest(stage: Stage){
        var district = stage.district?.id
        var backgrounds = stage.backgrounds?.map { BackgroundWithPositionForRest(it) }
        var wastes = stage.wastes?.map { WasteWithPositionForRest(it) }
        var wasteCollectors = stage.wasteCollectors?.map { CollectorWithPositionForRest(it) }
        var standardWasteCollectorsPosition = stage.standardWasteCollectorsPosition
        var stageNumber = stage.stageNumber
}


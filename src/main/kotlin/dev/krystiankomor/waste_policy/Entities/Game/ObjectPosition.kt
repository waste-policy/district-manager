package dev.krystiankomor.waste_policy.Entities.Game

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


interface ObjectPosition {
    var x: Long
    var y: Long
    var scale: Long
    var zIndex: Long
    var countFromRight: Boolean
    var countFromBottom: Boolean
}


@Entity
data class StandartObjectPosition(
        override var x: Long = 0,
        override var y: Long = 0,
        override var scale: Long = 0,
        override var zIndex: Long = 0,
        override var countFromRight: Boolean = false,
        override var countFromBottom: Boolean = false,

        @Id
        @GeneratedValue
        var id: Long? = null
) : ObjectPosition
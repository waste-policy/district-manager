package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import javax.persistence.Id
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.ManyToOne


@Entity
data class Background(@ManyToOne val image: ImageStorage = ImageStorage()){
        @Id
        @GeneratedValue
        var id: Long = 0
}
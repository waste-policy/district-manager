package dev.krystiankomor.waste_policy.Entities.Game

import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class BackgroundWithPosition(
        @ManyToOne
        var image: ImageStorage? = null,

        override var x: Long = 0,
        override var y: Long = 0,
        override var scale: Long = 0,
        override var zIndex: Long = 0,
        override var countFromRight: Boolean = false,
        override var countFromBottom: Boolean = false,

        @Id
        @GeneratedValue
        var id: Long? = null

) : ObjectPosition

class BackgroundWithPositionForRest(backgroundWithPosition: BackgroundWithPosition){
        var imageId = backgroundWithPosition.image?.id
        var x = backgroundWithPosition.x
        var y = backgroundWithPosition.y
        var zIndex = backgroundWithPosition.zIndex
        var scale = backgroundWithPosition.scale
        var countFromRight = backgroundWithPosition.countFromRight
        var countFromBottom = backgroundWithPosition.countFromBottom
}
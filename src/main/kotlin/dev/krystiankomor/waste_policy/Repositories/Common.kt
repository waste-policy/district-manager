package dev.krystiankomor.waste_policy.Repositories

import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import org.springframework.data.repository.CrudRepository

interface ColorRepository : CrudRepository<Color, Long>

interface ImageStorageRepository : CrudRepository<ImageStorage, Long>

package dev.krystiankomor.waste_policy.Repositories

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.Voivodeship
import org.springframework.data.repository.CrudRepository

interface VoivodeshipRepository : CrudRepository<Voivodeship, Long>

interface DistrictRepository : CrudRepository<District, Long>

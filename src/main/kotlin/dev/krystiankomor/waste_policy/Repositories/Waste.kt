package dev.krystiankomor.waste_policy.Repositories

import dev.krystiankomor.waste_policy.Entities.Waste.Waste
import dev.krystiankomor.waste_policy.Entities.Waste.Category
import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import org.springframework.data.repository.CrudRepository

interface CategoryRepository : CrudRepository<Category, Long>

interface WasteRepository : CrudRepository<Waste, Long>

interface CollectorRepository : CrudRepository<Collector, Long> {
    fun findAllByDistrictId(id: Long): List<Collector>
    fun findByDistrictIdAndId(districtId: Long, id: Long): Collector?
}
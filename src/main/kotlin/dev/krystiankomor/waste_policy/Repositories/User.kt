package dev.krystiankomor.waste_policy.Repositories

import dev.krystiankomor.waste_policy.Entities.User.Role
import dev.krystiankomor.waste_policy.Entities.User.User
import org.springframework.data.repository.CrudRepository


interface UserRepository : CrudRepository<User, Long>

interface RoleRepository : CrudRepository<Role, Long>
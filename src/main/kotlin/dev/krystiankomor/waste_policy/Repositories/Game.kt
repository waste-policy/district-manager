package dev.krystiankomor.waste_policy.Repositories

import dev.krystiankomor.waste_policy.Entities.Game.*
import org.springframework.data.repository.CrudRepository

interface BackgroundRepository : CrudRepository<Background, Long>

interface BackgroundWithPositionRepository : CrudRepository<BackgroundWithPosition, Long>

interface StageRepository : CrudRepository<Stage, Long>

interface WasteWithPositionRepository : CrudRepository<WasteWithPosition, Long>

interface CollectorWithPositionRepository : CrudRepository<CollectorWithPosition, Long>

interface StandartObjectPositionRepository : CrudRepository<StandartObjectPosition, Long>
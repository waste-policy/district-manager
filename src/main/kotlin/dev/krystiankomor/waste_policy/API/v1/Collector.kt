package dev.krystiankomor.waste_policy.API.v1

import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import dev.krystiankomor.waste_policy.Entities.Waste.CollectorTypeEnum
import dev.krystiankomor.waste_policy.Repositories.CollectorRepository
import dev.krystiankomor.waste_policy.Repositories.DistrictRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.validation.Valid


@RestController
@RequestMapping("/v1/district-management/{district-id}/collectors")
class Collector(val collectorRepository: CollectorRepository){

    @GetMapping
    fun findAllByDistrictId(@PathVariable("district-id") districtId: Long)
            = collectorRepository.findAllByDistrictId(districtId)

    @GetMapping("/{collector-id}")
    fun findByDistrictIdAndCollectorId(
            @PathVariable("district-id") districtId: Long,
            @PathVariable("collector-id") collectorId: Long
    ) = collectorRepository.findByDistrictIdAndId(districtId, collectorId)

    @PostMapping
    fun create(@Valid @RequestBody collector: Collector): ResponseEntity<Collector> {
        val createdCollector = collectorRepository.save(collector)
        val uriLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdCollector.id)
                .toUri()

        return ResponseEntity.created(uriLocation).body(createdCollector)
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody collector: Collector): ResponseEntity<Collector> {
        val updatedCollector = collectorRepository
                .findByIdOrNull(id) ?: return ResponseEntity.notFound().build()

        updatedCollector.apply {
            name = collector.name
            categories = collector.categories
            type = collector.type
            color = collector.color
        }

        return ResponseEntity.ok(collectorRepository.save(updatedCollector))
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<HttpStatus> {
        val collectorToDelete = collectorRepository.findByIdOrNull(id) ?: return ResponseEntity.notFound().build()

        collectorRepository.delete(collectorToDelete)

        return ResponseEntity.noContent().build()
    }
}

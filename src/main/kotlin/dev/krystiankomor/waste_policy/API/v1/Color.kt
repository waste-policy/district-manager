package dev.krystiankomor.waste_policy.API.v1

import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Repositories.ColorRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.validation.Valid


@RestController
@RequestMapping("/v1/colors")
class Color(private val colorRepository: ColorRepository) {

    @GetMapping
    fun findAll() =
            colorRepository
                    .findAll()
                    .sortedByDescending { it.id }

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long) =
            colorRepository.findById(id)

    @PostMapping
    fun create(@Valid @RequestBody newColor: Color): ResponseEntity<Color> {
        val createdColor = colorRepository.save(newColor)
        val uriLocation = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdColor.id)
                .toUri()

        return ResponseEntity.created(uriLocation).body(createdColor)
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody newColor: Color): ResponseEntity<Color> {
        val updatedColor = colorRepository
                .findByIdOrNull(id) ?: return ResponseEntity.notFound().build()

        updatedColor.apply {
                name = newColor.name
                value = newColor.value
            }

        return ResponseEntity.ok(colorRepository.save(updatedColor))
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<HttpStatus> {
        val colorToDelete = colorRepository.findByIdOrNull(id) ?: return ResponseEntity.notFound().build()

        colorRepository.delete(colorToDelete)

        return ResponseEntity.noContent().build()
    }
}

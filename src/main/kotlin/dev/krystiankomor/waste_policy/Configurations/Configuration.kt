package dev.krystiankomor.waste_policy.Configurations

import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.District
import dev.krystiankomor.waste_policy.Entities.AdministrativeDivision.Voivodeship
import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Entities.Common.ImageStorage
import dev.krystiankomor.waste_policy.Entities.Common.ImageStorageType
import dev.krystiankomor.waste_policy.Entities.Game.*
import dev.krystiankomor.waste_policy.Entities.User.Role
import dev.krystiankomor.waste_policy.Entities.User.User
import dev.krystiankomor.waste_policy.Entities.Waste.Category
import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import dev.krystiankomor.waste_policy.Entities.Waste.CollectorTypeEnum
import dev.krystiankomor.waste_policy.Entities.Waste.Waste
import dev.krystiankomor.waste_policy.Repositories.*
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Configuration {

    @Bean
    fun databaseInitializer(wasteRepository: WasteRepository,
                            categoryRepository: CategoryRepository,
                            collectorRepository: CollectorRepository,

                            voivodeshipRepository: VoivodeshipRepository,
                            districtRepository: DistrictRepository,

                            colorRepository: ColorRepository,
                            imageStorageRepository: ImageStorageRepository,

                            backgroundWithPositionRepository: BackgroundWithPositionRepository,
                            stageRepository: StageRepository,
                            wasteWithPositionRepository: WasteWithPositionRepository,
                            collectorWithPositionRepository: CollectorWithPositionRepository,
                            standartObjectPositionRepository: StandartObjectPositionRepository,

                            roleRepository: RoleRepository,
                            userRepository: UserRepository
    ) = ApplicationRunner {
        val url = "https://krystiankomor.dev"

        val voivodeships = arrayListOf(
                Voivodeship("Dolnośląskie"),
                Voivodeship("Kujawsko-Pomorskie"),
                Voivodeship("Lubelskie"),
                Voivodeship("Lubuskie"),
                Voivodeship("Łódzkie"),
                Voivodeship("Małopolskie"),
                Voivodeship("Mazowieckie"),
                Voivodeship("Opolskie"),
                Voivodeship("Podkarpackie"),
                Voivodeship("Podlaskie"),
                Voivodeship("Pomorskie"),
                Voivodeship("Śląskie"),
                Voivodeship("Świętokrzyskie"),
                Voivodeship("Warmińsko-Mazurskie"),
                Voivodeship("Wielkopolskie"),
                Voivodeship("Zachodniopomorskie")
        ).map { voivodeshipRepository.save(it) }

        val districts = arrayListOf(
                District("Turawa", voivodeships[7])
        ).map { districtRepository.save(it) }

        val colors = arrayListOf(
                Color("Żółty", "fee900"),
                Color("Zielony", "326306"),
                Color("Czarny", "333333"),
                Color("Brązowy", "624a2e"),
                Color("Niebieski", "00adfe")
        ).map { colorRepository.save(it) }

        val images = arrayListOf(
                ImageStorage("$url/images/wastes/banana.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/batteries.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/bone.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/bottle.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/broken_glass.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/bulb.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/can.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/CD_disc.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/chips_package.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/jar.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/leaves.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/milk_carton.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/newspaper.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/smartphone.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/wastes/wardrobe.png", ImageStorageType.EXTERNAL),

                ImageStorage("$url/images/other-waste-collectors/pszok.png", ImageStorageType.EXTERNAL),

                ImageStorage("$url/images/backgrounds/background_default.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/backgrounds/cloud.svg", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/backgrounds/clover.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/backgrounds/grass.svg", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/backgrounds/tree.png", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/backgrounds/sun.svg", ImageStorageType.EXTERNAL),
                ImageStorage("$url/images/bags/bag-blue.svg", ImageStorageType.EXTERNAL)
        ).map { imageStorageRepository.save(it) }

        val categories = arrayListOf(
                Category("Papier", ""),
                Category("Metale", ""),
                Category("Tworzywa sztuczne", ""),
                Category("Opakowania wielomateriałowe", ""),
                Category("Bioodpady", "Odpady ulegające biodegradacji"),
                Category("Odpady zielone", ""),
                Category("Odpady zmieszane", ""),
                Category("Odpady wielkogabarytowe", ""),
                Category("Szkło bezbarwne", ""),
                Category("Szkło kolorowe", ""),
                Category("Odpady niebezpieczne", ""),
                Category("Elektrośmieci", ""),
                Category("Leki", "")
        ).map { categoryRepository.save(it) }

        val wastes = arrayListOf(
                Waste("Puszka", images[6], categories[1]),
                Waste("Paczka po chipsach", images[8], categories[3]),
                Waste("Karton po mleku", images[11], categories[3]),
                Waste("Gazeta", images[12], categories[0]),
                Waste("Banan", images[0], categories[4]),
                Waste("Baterie", images[1], categories[11]),
                Waste("Kość", images[2], categories[6]),
                Waste("Butelka zabarwiona", images[3], categories[9]),
                Waste("Stłuczona szklanka", images[4], categories[6]),
                Waste("Zarówka", images[5], categories[11]),
                Waste("Płyta CD/DVD", images[7], categories[11]),
                Waste("Słoik", images[9], categories[8]),
                Waste("Liście", images[10], categories[4]),
                Waste("Telefon komórkowy", images[13], categories[11]),
                Waste("Szafa", images[14], categories[7])
        ).map { wasteRepository.save(it) }

        val collectors = arrayListOf(
                Collector("Żółty pojemnik", "", "", "", colors[0], setOf(categories[1], categories[2], categories[3]), districts[0], CollectorTypeEnum.BIN),
                Collector("Zielony pojemnik", "", "", "", colors[1], setOf(categories[8], categories[9]), districts[0], CollectorTypeEnum.BIN),
                Collector("Niebieski worek", "", "", "", colors[4], setOf(categories[0]), districts[0], CollectorTypeEnum.BAG),
                Collector("Czarny pojemnik", "", "", "", colors[2], setOf(categories[6]), districts[0], CollectorTypeEnum.BIN),
                Collector("Brązowy pojemnik", "", "", "", colors[3], setOf(categories[4]), districts[0], CollectorTypeEnum.BIN),
                Collector("PSZOK", "Punkt Selektywnej Zbiórki Odpadów Komunalnych", "Wodna 15, Kotórz Mały", "Na przeciwko oczyszczalni ścieków", null, categories.toSet(), districts[0],CollectorTypeEnum.ELECTRO_WASTES_POINT, images[15])
        ).map { collectorRepository.save(it) }

        val backgrounds = arrayListOf(
                /* base */
                BackgroundWithPosition(images[16], 0, 0, 100, 0, countFromRight = false, countFromBottom = false),
                /*sun*/
                BackgroundWithPosition(images[21], 0, 0, 40, 1, countFromRight = false, countFromBottom = false),
                /*cloud*/
                BackgroundWithPosition(images[17], 60, 10, 15, 2, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[17], 20, 5, 20, 2, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[17], 80, 0, 30, 2, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[17], 5, 20, 10, 2, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[17], 40, -10, 15, 2, countFromRight = false, countFromBottom = false),
                /*tree*/
                BackgroundWithPosition(images[20], -5, 0, 55, 3, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[20], 10, 5, 30, 3, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[20], 20, 7, 35, 3, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[20], 35, 15, 30, 3, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[20], 45, 3, 35, 3, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[20], 60, 10, 30, 3, countFromRight = true, countFromBottom = false),
                /*grass*/
                BackgroundWithPosition(images[19], 18, 30, 5, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 20, 43, 5, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 30, 45, 8, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 35, 40, 5, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 50, 40, 10, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 43, 35, 7, 4, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[19], 13, 48, 5, 4, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[19], 5, 37, 10, 4, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[19], 30, 40, 8, 4, countFromRight = false, countFromBottom = false),
                /* clover */
                BackgroundWithPosition(images[18], 40, 35, 7, 5, countFromRight = false, countFromBottom = false),
                BackgroundWithPosition(images[18], 43, 45, 5, 5, countFromRight = true, countFromBottom = false),
                BackgroundWithPosition(images[18], 20, 40, 8, 5, countFromRight = false, countFromBottom = false)
        ).map{ backgroundWithPositionRepository.save(it) }

        val wastesWithPosition = arrayListOf(
                WasteWithPosition(wastes[0], 20, 50, 8, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[1], 50, 35, 8, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[2], 10, 35, 12, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[3], 75, 60, 6, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[4], 8, 70, 8, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[5], 50, 50, 7, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[6], 30, 50, 4, 10, countFromRight = true, countFromBottom = true),
                WasteWithPosition(wastes[7], 15, 10, 15, 10, countFromRight = true, countFromBottom = true),
                WasteWithPosition(wastes[8], 55, 60, 8, 10, countFromRight = false, countFromBottom = false)
        ).map { wasteWithPositionRepository.save(it) }

        val wastesWithPosition2 = arrayListOf(
                WasteWithPosition(wastes[6], 20, 50, 17, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[7], 50, 35, 8, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[8], 10, 35, 8, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[9], 75, 60, 6, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[10], 8, 70, 6, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[11], 50, 50, 10, 10, countFromRight = false, countFromBottom = false),
                WasteWithPosition(wastes[12], 5, 40, 10, 10, countFromRight = true, countFromBottom = true),
                WasteWithPosition(wastes[13], 15, 10, 15, 10, countFromRight = true, countFromBottom = true),
                WasteWithPosition(wastes[14], 75, 60, 6, 10, countFromRight = false, countFromBottom = false)
        ).map { wasteWithPositionRepository.save(it) }

        val collectorsWithPosition = arrayListOf(
                CollectorWithPosition(collectors[5], 5, 35, 20, 20, countFromRight = true, countFromBottom = false)
        ).map { collectorWithPositionRepository.save(it) }

        val standartObjectsPosition = arrayListOf(
                StandartObjectPosition(2, 5, 20, 10, false, countFromBottom = true),
                StandartObjectPosition(2, 5, 20, 10, false, countFromBottom = true)
        ).map { standartObjectPositionRepository.save(it) }

        val stages = arrayListOf(
                Stage(
                        backgrounds.toSet(),
                        wastesWithPosition.toSet(),
                        standartObjectsPosition[0],
                        collectorsWithPosition.toSet(),
                        districts[0],
                        1),
                Stage(
                        backgrounds.toSet(),
                        wastesWithPosition2.toSet(),
                        standartObjectsPosition[1],
                        collectorsWithPosition.toSet(),
                        districts[0],
                        2)
        ).map { stageRepository.save(it) }

//        val roles = arrayListOf(
//                Role("Administrator gminy"),
//                Role("Administrator systemu")
//        ).map { roleRepository.save(it) }
//
//        val users = arrayListOf(
//                User("krystian.komor@gmail.com", "krystian", roles[1], null),
//                User("admin@turawa.pl", "turawa", roles[0], districts[0])
//        ).map { userRepository.save(it) }
    }
}
package dev.krystiankomor.waste_policy

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/api/**")
                .allowedMethods("*")
//                .allowedOrigins("https://game.krystiankomor.dev", "https://panel.krystiankomor.dev", "https://wyszukaj.krystiankomor.dev")
    }

}
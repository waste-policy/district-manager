package dev.krystiankomor.waste_policy.HttpControllers

import dev.krystiankomor.waste_policy.Entities.Common.Color
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class ColorCrudController {

    @GetMapping("color/add")
    fun showAddColor(color: Color): String {
        return "add-color"
    }
}
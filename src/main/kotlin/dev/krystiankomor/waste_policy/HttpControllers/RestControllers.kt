package dev.krystiankomor.waste_policy.HttpControllers

import dev.krystiankomor.waste_policy.Entities.Common.Color
import dev.krystiankomor.waste_policy.Entities.Game.Game
import dev.krystiankomor.waste_policy.Entities.Game.StageForRest
import dev.krystiankomor.waste_policy.Entities.Waste.Collector
import dev.krystiankomor.waste_policy.Entities.Waste.CollectorTypeEnum
import dev.krystiankomor.waste_policy.Entities.Waste.Waste
import dev.krystiankomor.waste_policy.Repositories.*
import org.apache.coyote.Response
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/waste")
class WasteController(private val repository: WasteRepository,
                      private val imageStorageRepository: ImageStorageRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll().sortedBy { it.image != null}.sortedByDescending { it.id }

    @PostMapping
    fun create(@Valid @RequestBody waste: Waste): ResponseEntity<Waste> {
        if(waste.image != null) {
            imageStorageRepository.findById(waste.image?.id!!)
        }
        return ResponseEntity.ok(repository.save(waste))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") wasteId: Long, @Valid @RequestBody putWaste: Waste): ResponseEntity<Waste> {
        return repository.findById(wasteId).get().let { waste ->
            waste.name = putWaste.name
            waste.category = putWaste.category

            if (putWaste.image != null) {
                waste.image = putWaste.image
            }

            return@let  ResponseEntity.ok(repository.save(waste))
        }
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") wasteId: Long): ResponseEntity<Waste> {
        repository.deleteById(wasteId)

        return ResponseEntity.noContent().build()
    }

}

@RestController
@RequestMapping("/api/color")
class ColorController(private val repository: ColorRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll().sortedByDescending { it.id }

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") colorId: Long) = repository.findById(colorId)

    @PostMapping
    fun create(@Valid @RequestBody waste: Color): ResponseEntity<Color> {
        return ResponseEntity.ok(repository.save(waste))
    }

    @PutMapping("/")
    fun update(/*@PathVariable("id") colorId: Long,*/ @Valid @RequestBody putColor: Color): ResponseEntity<Color> {
//        val colorToUpdate = repository.findById(colorId).get().copy(name = putColor.name, value = putColor.value)

        return  ResponseEntity.ok(repository.save(putColor))
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") wasteId: Long): ResponseEntity<Waste> {
        repository.deleteById(wasteId)

        return ResponseEntity.noContent().build()
    }


}

@RestController
@RequestMapping("/api/category")
class Category(private val repository: CategoryRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll()

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") categoryId: Long) = repository.findById(categoryId)

}

@RestController
@RequestMapping("/api/district")
class DistrictController(private val repository: DistrictRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll()

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") districtId: Long) = repository.findById(districtId)
}

@RestController
@RequestMapping("/api/collector")
class CollectorController(private val repository: CollectorRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll()

    @GetMapping("/{district}")
    fun findByDistrictId(@PathVariable("district") districtId: Long) : List<Collector> {
        return repository
                .findAll()
                .filter { collector -> collector.district?.id?.equals(districtId)!! }
                .filter { collector -> listOf(CollectorTypeEnum.BIN, CollectorTypeEnum.BAG).contains(collector.type) }
                .sortedByDescending { it.id }
    }

    @PostMapping
    fun create(@Valid @RequestBody collector: Collector): ResponseEntity<Collector> {
        return ResponseEntity.ok(repository.save(collector))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") collectorId: Long, @Valid @RequestBody putCollector: Collector): ResponseEntity<Collector> {
        return repository.findById(collectorId).get().let { collector ->
            collector.name = putCollector.name
            collector.categories = putCollector.categories
            collector.type = putCollector.type
            collector.color = putCollector.color

            return@let  ResponseEntity.ok(repository.save(collector))
        }
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") collectorId: Long): ResponseEntity<Collector> {
        repository.deleteById(collectorId)

        return ResponseEntity.noContent().build()
    }


}

@RestController
@RequestMapping("/api/stage")
class StageController(private val repository: StageRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll()

    @GetMapping("/all/{district}")
    fun findByDistrictId(@PathVariable("district") districtId: Long) : List<StageForRest> {
        return repository
                .findAll()
                .filter { stage -> stage.district?.id?.equals(districtId)!! }
                .map { StageForRest(it) }
    }

}

@RestController
@RequestMapping("/api/image")
class ImageController(private val repository: ImageStorageRepository){

    @GetMapping("/all")
    fun findAll() = repository.findAll()

}

@RestController
@RequestMapping("/api/game")
class GameController(private val wasteRepository: WasteRepository,
                     private val collectorRepository: CollectorRepository,
                     private val districtRepository: DistrictRepository,
                     private val stageRepository: StageRepository){

    @GetMapping("/{district}")
    fun findByDistrictId(@PathVariable("district") districtId: Long) : Game {
        val collectors = collectorRepository
                .findAll()
                .filter { collector -> collector.district?.id?.equals(districtId)!! }
                .toList()

        for(collector in collectors) {
            collector.generateImageUrl()
        }

        val wastes = wasteRepository
                .findAll()
                .filter{ it.image != null && it.image!!.id != null }
                .toList()

        val district = districtRepository.findById(districtId).get()

        val stages = stageRepository
                .findAll()
                .filter { stage -> stage.district?.id?.equals(districtId)!! }
                .toList()

        return Game(wastes, collectors, stages, district)
    }

}


@RestController
@RequestMapping("/api/find")
class FindCollectorController(private val wasteRepository: WasteRepository,
                     private val collectorRepository: CollectorRepository){

    @GetMapping("/{district}/{waste_name}")
    fun findByDistrictId(@PathVariable("district") districtId: Long,
                         @PathVariable("waste_name") wasteName: String) : List<Collector> {
        val wastes = wasteRepository.findAll().filter { waste -> waste.name.contains(wasteName, ignoreCase = true) }

        val collectors = wastes.map { waste -> collectorRepository.findAll().filter { collector -> collector.categories.contains(waste.category) } }

        return collectors.flatten()
    }

}

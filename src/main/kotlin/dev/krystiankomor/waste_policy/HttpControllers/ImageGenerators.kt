package dev.krystiankomor.waste_policy.HttpControllers

import dev.krystiankomor.waste_policy.Generators.ImageTemplates
import org.springframework.core.io.InputStreamResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse


@RestController
@RequestMapping("/api/generator")
class ImageGenerators(val imageTemplates: ImageTemplates) {

    @RequestMapping("/bin/{color}.svg")
    fun getBinImage(@PathVariable("color") color: String, response: HttpServletResponse): ResponseEntity<InputStreamResource> {
        return ResponseEntity
                .ok()
                .header("ContentType", "image/svg+xml")
                .body(InputStreamResource(imageTemplates.getBinTemplate(color).toByteArray().inputStream()))
    }

    @RequestMapping("/bag/{color}.svg")
    fun getBagImage(@PathVariable("color") color: String, response: HttpServletResponse): ResponseEntity<InputStreamResource> {
        return ResponseEntity
                .ok()
                .header("ContentType", "image/svg+xml")
                .body(InputStreamResource(imageTemplates.getBagTemplate(color).toByteArray().inputStream()))
    }
}
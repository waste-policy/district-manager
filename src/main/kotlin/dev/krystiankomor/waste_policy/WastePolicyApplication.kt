package dev.krystiankomor.waste_policy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@SpringBootApplication
class WastePolicyApplication

fun main(args: Array<String>) {
	runApplication<WastePolicyApplication>(*args)
}

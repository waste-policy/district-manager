package dev.krystiankomor.waste_policy.Initializers

import dev.krystiankomor.waste_policy.WastePolicyApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

class ServletInitializer : SpringBootServletInitializer() {

	override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
		return application.sources(WastePolicyApplication::class.java)
	}

}
